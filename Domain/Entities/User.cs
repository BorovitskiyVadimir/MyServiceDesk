﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyServiceDesk.Domain.Entities
{
    public class User: IdentityUser
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string Name { get; set; }
        
        [Required]
        [Display(Name = "Фамилия пользователя")]
        public string SurName { get; set; }

        [Display(Name = "Отчество пользователя")]
        public string MiddleName { get; set; }
    }
}
